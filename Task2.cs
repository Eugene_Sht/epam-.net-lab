using System;
using System.Text;

namespace EPAM_NET_Lab
{
    /// <summary>
    /// Structure Point represents a point with X and Y coordinates
    /// </summary>
    struct Point
    {
        public double X;
        public double Y;
        public Point(double x, double y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Returns value that shows wether current Point instance is equal to specified one
        /// </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is Point))
            {
                return false;
            }
            Point other = (Point)obj;
            return (X == other.X) && (Y == other.Y);
        }

        /// <summary>
        /// Returns hash code for current instance 
        /// </summary>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        /// <summary>
        /// Returns string representation of current Point instance 
        /// </summary>
        public override string ToString()
        {
            return string.Format("({0}, {1})", X, Y);
        }

        /// <summary>
        /// Determines the equality of two points
        /// </summary>
        public static bool operator ==(Point pt1, Point pt2)
        {
            return pt1.Equals(pt2);
        }

        /// <summary>
        /// Determines the equality of two points
        /// </summary>
        public static bool operator !=(Point pt1, Point pt2)
        {
            return !pt1.Equals(pt2);
        }
    }

    /// <summary>
    /// Class Rectangle represents rectangle with sides parallel to coordinate axes
    /// </summary>
    class Rectangle
    {
        Point p1;
        Point p2;
        Point p3;
        Point p4;

        /// <param name="x1">X-coordinate of bottom left point of rectangle</param>
        /// <param name="y1">Y-coordinate of bottom left point of rectangle</param>
        /// <param name="x2">X-coordinate of top right point of rectangle</param>
        /// <param name="y2">Y-coordinate of top right point of rectangle</param>
        public Rectangle(double x1, double y1, double x2, double y2)
        {
            p1.X = x1;
            p1.Y = y1;
            p2.X = x1;
            p2.Y = y2;
            p3.X = x2;
            p3.Y = y2;
            p4.X = x2;
            p4.Y = y1;
        }

        /// <param name="pt1">Structure of type Point representing the bottom left point of rectangle</param>
        /// <param name="pt2">Structure of type Point representing the top right point of rectangle</param>
        public Rectangle(Point pt1, Point pt2)
        {
            p1 = pt1;
            p2.X = pt1.X;
            p2.Y = pt2.Y;
            p3 = pt2;
            p4.X = pt2.X;
            p4.Y = pt1.Y;
        }

        /// <summary>
        /// Returns an array of Point objects pairs representing extreme points of the rectangle
        /// </summary>
        public Point[] GetPoints
        {
            get
            {
                return new Point[4] { p1, p2, p3, p4 };
            }
        }

        /// <summary>
        /// Gets height of the rectangle
        /// </summary>
        public double Height
        {
            get
            {
                return p2.Y - p1.Y;
            }
        }

        /// <summary>
        /// Gets width of the Rectangle
        /// </summary>
        public double Width
        {
            get
            {
                return p4.X - p1.X;
            }
        }

        /// <summary>
        /// Gets object of type Point representing the bottom left point of the rectangle
        /// </summary>
        public Point GetBottomLeft
        {
            get
            {
                return p1;
            }
        }

        /// <summary>
        /// Gets object of type Point representing the top right point of the rectangle
        /// </summary>
        public Point GetTopRight
        {
            get
            {
                return p3;
            }
        }

        /// <summary>
        /// Gets object of type Point representing the bottom right point of the rectangle
        /// </summary>
        public Point GetBottomRight
        {
            get
            {
                return p4;
            }
        }

        /// <summary>
        /// Gets object of type Point representing the top left point of the rectangle
        /// </summary>
        public Point GetTopLeft
        {
            get
            {
                return p2;
            }
        }

        /// <summary>
        /// Gets object of type Point representing of the center of the rectangle
        /// </summary>
        public Point GetCenter
        {
            get
            {
                return new Point() { X = (p4.X - p1.X) / 2 + p1.X, Y = (p2.Y - p1.Y) / 2 + p1.Y };
            }
        }

        /// <summary>
        /// Gets the square of the rectangle
        /// </summary>
        public double GetSquare
        {
            get
            {
                return Height * Width;
            }
        }

        /// <summary>
        /// Gets the perimeter of the rectangle
        /// </summary>
        public double GetPerimeter
        {
            get
            {
                return 2 * (Height + Width);
            }
        }

        /// <summary>
        /// Determines the equality of two rectangles
        /// </summary>
        public static bool operator ==(Rectangle A, Rectangle B)
        {
            if ((object)A == (object)B)
            {
                return true;
            }
            if ((object)A == null || (object)B == null)
            {
                return false;
            }
            return A.Equals(B);
        }

        /// <summary>
        /// Determines the inequality of two rectangles
        /// </summary>
        public static bool operator !=(Rectangle A, Rectangle B)
        {
            if ((object)A == (object)B)
            {
                return true;
            }
            if ((object)A == null || (object)B == null)
            {
                return false;
            }
            return !A.Equals(B);
        }

        /// <summary>
        /// Returns the smallest possible rectangle that contains two specified rectangles
        /// </summary>
        public static Rectangle LeastRect(Rectangle A, Rectangle B)
        {
            if ((object)A == null || (object)B == null)
            {
                throw new NullReferenceException("One or both operands are equal to null");
            }
            double[] XPoints = new double[8];
            double[] YPoints = new double[8];
            Point[] PointsA = A.GetPoints;
            Point[] PointsB = B.GetPoints;
            double minX;
            double minY;
            double maxX;
            double maxY;

            for (var i = 0; i < 4; i++)
            {
                XPoints[2 * i] = PointsA[i].X;
                XPoints[2 * i + 1] = PointsB[i].X;
                YPoints[2 * i] = PointsA[i].Y;
                YPoints[2 * i + 1] = PointsB[i].Y;
            }

            minX = XPoints[0];
            for (var i = 0; i < 8; i++)
            {
                if (minX > XPoints[i])
                    minX = XPoints[i];
            }

            maxX = XPoints[0];
            for (var i = 0; i < 8; i++)
            {
                if (maxX < XPoints[i])
                    maxX = XPoints[i];
            }

            minY = YPoints[0];
            for (var i = 0; i < 8; i++)
            {
                if (minY > YPoints[i])
                    minY = YPoints[i];
            }

            maxY = YPoints[0];
            for (var i = 0; i < 8; i++)
            {
                if (maxY < YPoints[i])
                    maxY = YPoints[i];
            }

            return new Rectangle(minX, minY, maxX, maxY);
        }

        /// <summary>
        /// Returns the rectangle, made by crossing of two specified rectangles
        /// </summary>
        public static Rectangle CrossRectangle(Rectangle A, Rectangle B)
        {
            if ((object)A == null || (object)B == null)
            {
                throw new NullReferenceException("One or both operands are equal to null");
            }

            if ((A.GetBottomLeft.X > B.GetTopRight.X) || (A.GetTopRight.X < B.GetBottomLeft.X)
                || (A.GetBottomLeft.Y > B.GetTopRight.Y) || (A.GetTopRight.Y < B.GetBottomLeft.Y))
            {
                return null;				// There is no crossing
            }
            else
            {
                Point newP1 = new Point() { X = Math.Max(A.GetBottomLeft.X, B.GetBottomLeft.X), Y = Math.Max(A.GetBottomLeft.Y, B.GetBottomLeft.Y) };
                Point newP2 = new Point() { X = Math.Min(A.GetTopRight.X, B.GetTopRight.X), Y = Math.Min(A.GetTopRight.Y, B.GetTopRight.Y) };
                return new Rectangle(newP1, newP2);
            }
        }

        /// <summary>
        /// Returns a new Rectangle instance, which is the result of moving rectangle A to specified location
        /// </summary>
        /// <param name="newXBottomLeft">Point type representing a new bottom left point of rectangle</param>
        public static Rectangle Move(Rectangle A, Point newBottomLeft)
        {
            if ((object)A == null)
            {
                throw new NullReferenceException("The operands is equal to null");
            }
            return new Rectangle(newBottomLeft.X, newBottomLeft.Y, A.GetTopRight.X + (newBottomLeft.X - A.GetBottomLeft.X), A.GetTopRight.X + (newBottomLeft.Y - A.GetBottomLeft.Y));
        }

        /// <summary>
        /// Returns a new Rectangle instance, which is resized by moving bottom left point of the rectangle A
        /// </summary>
        /// <param name="newXcoord">Point type representing a new bottom left point of rectangle</param>
        public static Rectangle Resize(Rectangle A, Point newBottomLeft)
        {
            if ((object)A == null)
            {
                throw new NullReferenceException("The operands is equal to null");
            }
            return new Rectangle(newBottomLeft.X, newBottomLeft.Y, A.GetTopRight.X, A.GetTopRight.Y);
        }

        /// <summary>
        /// Returns a new Rectangle instance, which is resized 
        /// with respect to the central point of rectangle A by a specified factor
        /// </summary>
        public static Rectangle Resize(Rectangle A, double factor)
        {
            if ((object)A == null)
            {
                throw new NullReferenceException("The operands is equal to null");
            }
            double newLengthB = A.Width * factor;
            double newLengthA = A.Height * factor;
            Point centerPoint = A.GetCenter;
            Point newBottomLeft = new Point() { X = centerPoint.X - newLengthB / 2, Y = centerPoint.Y - newLengthA / 2 };
            Point newTopRight = new Point() { X = centerPoint.X + newLengthB / 2, Y = centerPoint.Y + newLengthA / 2 };

            return new Rectangle(newBottomLeft, newTopRight);
        }

        /// <summary>
        /// Returns a new Rectangle instance, which is moved to specified location
        /// </summary>
        /// <param name="newXBottomLeft">Point type representing a new bottom left point of rectangle</param>
        public Rectangle Move(Point newBottomLeft)
        {
            return new Rectangle(newBottomLeft.X, newBottomLeft.Y, newBottomLeft.X - p1.X, newBottomLeft.Y - p1.Y);
        }

        /// <summary>
        /// Returns a new Rectangle instance, which is resized by moving bottom left point
        /// </summary>
        /// <param name="newXcoord">New X-coordinate of bottom left point of rectangle</param>
        /// <param name="newYcoord">New X-coordinate of bottom left point of rectangle</param>
        public Rectangle Resize(Point newBottomLeft)
        {
            return new Rectangle(newBottomLeft.X, newBottomLeft.Y, p3.X, p3.Y);
        }

        /// <summary>
        /// Returns a new Rectangle instance, which is resized with respect to the central point by a specified factor
        /// </summary>
        public Rectangle Resize(double factor)
        {
            double newLengthB = Width * factor;
            double newLengthA = Height * factor;
            Point centerPoint = GetCenter;
            Point newBottomLeft = new Point() { X = centerPoint.X - newLengthB / 2, Y = centerPoint.Y - newLengthA / 2 };
            Point newTopRight = new Point() { X = centerPoint.X + newLengthB / 2, Y = centerPoint.Y + newLengthA / 2 };

            return new Rectangle(newBottomLeft, newTopRight);
        }

        /// <summary>
        /// Determines if current Rectangle instance is equal to another 
        /// </summary>
        public override bool Equals(object obj)
        {
            Rectangle other = obj as Rectangle;
            if (other == null)
            {
                return false;
            }

            return (this.GetBottomLeft == other.GetBottomLeft) && (this.GetTopRight == other.GetTopRight);
        }

        /// <summary>
        /// Returns hash code for current instance 
        /// </summary>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Returns string representation of current Rectangle instance 
        /// </summary>
        public override string ToString()
        {
            StringBuilder result = new StringBuilder(100);
            result.AppendLine("Rectangle: ");
            result.AppendLine(string.Format("Bottom left point : {0}", this.GetBottomLeft));
            result.AppendLine(string.Format("Top left point: {0}", this.GetTopLeft));
            result.AppendLine(string.Format("Top right point: {0}", this.GetTopRight));
            result.AppendLine(string.Format("Bottom right point: {0}", this.GetBottomRight));
            return result.ToString();
        }
    }

    class Program
    {
        /// <summary>
        /// Static method that demonstrates the usage of the Rectangle type
        /// </summary>
        public static void Main()
        {
            Rectangle A = new Rectangle(1, 1, 10, 5);
            Rectangle B = new Rectangle(5, 0.5, 13, 3);
            Rectangle C = Rectangle.LeastRect(A, B);
            Rectangle D = Rectangle.CrossRectangle(A, B);

            Console.WriteLine("\t\t\t\tEPAM .NET Lab");
            Console.WriteLine("\t\t\t\t    Task 2");
            Console.WriteLine("  Class Rectangle represents rectangle with sides parallel to coordinate axes");

            Console.WriteLine("\nRectangle A : \n{0}", A.ToString());
            Console.WriteLine("Center point of A : {0}", A.GetCenter);

            Console.WriteLine("\nRectangle B: \n{0}", B.ToString());
            Console.WriteLine("Center point of B: {0}", B.GetCenter);

            Console.WriteLine("\nSquare of A: S = {0}", A.GetSquare);
            Console.WriteLine("Perimeter of A: P = {0}", A.GetPerimeter);

            Console.WriteLine("\nWidth of A: W = {0}", A.Width);
            Console.WriteLine("Height of A: H = {0}", A.Height);

            Console.WriteLine("\nThe smallest possible rectangle C that contains rectangles A and B:");
            Console.WriteLine("\nRectangle C: \n{0}", C.ToString());
            Console.WriteLine("Center point of C: {0}", C.GetCenter);

            Console.WriteLine("\nThe rectangle D, made by crossing of two specified rectangles:");
            Console.WriteLine("\nRectangle D: \n{0}", D.ToString());

            Console.WriteLine("Move rectangle A 5 units to right");
            Console.WriteLine(Rectangle.Move(A, new Point { X = A.GetBottomLeft.X + 5, Y = A.GetBottomLeft.Y }));
            Console.WriteLine("Decrease rectangle A by 5 units");
            Console.WriteLine(Rectangle.Resize(A, new Point { X = A.GetBottomLeft.X + 5, Y = A.GetBottomLeft.Y }));
            Console.WriteLine("Decrease rectangle A by factor 0.5");
            Console.WriteLine(Rectangle.Resize(A, 0.5));
            Console.WriteLine("\nA == B: \t{0}", A == B);
            Console.WriteLine("\nA != B: \t{0}", A != B);
        }
    }
}